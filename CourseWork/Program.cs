﻿using ExcelDataReader;
using Constants = CourseWork.Constants;
    
public class Program
{
    public static void Main()
    {
        Console.WriteLine("Заполнение признаков, а так же классов признаков из таблицы");
        (double[][] sings, int[] ck, string[] singsName) = FillExcelData(7,12,0,6, 11);
        PrintTwoDimension(sings, singsName);
        PrintOneDimension(ck);
        Console.WriteLine(Constants.Line);
        
        Console.WriteLine("Нахождение минимального и максимального значений в каждом из признаков");
        var maxMinSings = FindMinMax(sings);
        PrintTwoDimension(maxMinSings, singsName);
        Console.WriteLine(Constants.Line);

        Console.WriteLine("Нормализация признаков");
        var normalizedSings = NormalizeSings(sings, maxMinSings);
        PrintTwoDimension(normalizedSings, singsName);
        Console.WriteLine(Constants.Line);

        Console.WriteLine("Нахождение границ; (min+max)/2");
        var middleValues = FindMiddleMaxMinSings(maxMinSings);
        PrintOneDimension(middleValues, singsName);
        Console.WriteLine(Constants.Line);
        
        Console.WriteLine("Частота появления признаков");
        var frequencies = FrequencyOfSingsOccurrences(normalizedSings, ck, middleValues);
        PrintFrequencies(frequencies, singsName);
        Console.WriteLine(Constants.Line);

        Console.WriteLine("Подсчет числа векторов во множестве данных");
        (int normalCount, int attackCount) = VectorCounts(normalizedSings, ck);
        Console.WriteLine(Constants.FormatSettings, $"Количество Normal {normalCount}");
        Console.WriteLine(Constants.FormatSettings, $"Количество Attack {attackCount}");
        Console.WriteLine(Constants.Line);

        double[][] updatedFrequencies = frequencies;
        string[] updatedSingsName = singsName;
        string[] rulesWithName = new string[Constants.SingsCount];
        string[] rules = new string[Constants.SingsCount];
        string[] topSingsName = new String[Constants.SingsCount];
        for (int i = 0; i < Constants.SingsCount; i++)
        {
            (updatedFrequencies, updatedSingsName, rulesWithName[i], topSingsName[i], rules[i])
                = ExtractMaxFrequenciesOfClassSings(updatedFrequencies, updatedSingsName);
            if (updatedFrequencies.Length > 0)
            {
                PrintTwoDimension(updatedFrequencies, updatedSingsName);
            }
        }
        
        Console.WriteLine(Constants.Line);
        Console.WriteLine("Вывод всех правил");
        for (int i = 0; i < rulesWithName.Length; i++)
        {
            Console.WriteLine(rulesWithName[i]);
        }
        Console.WriteLine(Constants.Line);


        var result = Tree(normalizedSings, FindTopOrder(topSingsName, singsName), rules);
        CompareClassSingsResultWithOriginal(ck, result);
        Console.ReadKey();
    }

    public static int[] FindTopOrder(string[] topSingsWithName, string[] singsNames)
    {
        int[] topOrder = new int[singsNames.Length-1];
        for (int i = 0; i < singsNames.Length-1; i++)
        {
            for (int j = 0; j < singsNames.Length-1; j++)
            {
                if (topSingsWithName[i] == singsNames[j])
                {
                    topOrder[i] = j;
                }
            }
        }

        return topOrder;
    }

    public static (double[][], int[], string[]) FillExcelData(params int[] parameters)
    {
        Console.WriteLine();
        Constants.SingsCount = parameters.Length;
        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        using (var stream = File.Open("1.xlsx", FileMode.Open, FileAccess.Read))
        {
            IExcelDataReader reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);
            var conf = new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = false
                }
            };

            var dataSet = reader.AsDataSet(conf);
            var dataTable = dataSet.Tables[0];

            var singsName = new string[Constants.SingsCount + 1]; // 1 for attack type
            singsName[^1] = "AttackType";
            var sings = AllocateMemory(Constants.SingsCount, Constants.LengthSings);

            // Нулевые строки это заголовки
            for (int singsi = 0; singsi < parameters.Length; singsi++)
            {
                for (int j = 1,singsj=0; j <= Constants.LengthSings; ++j, singsj++)
                {
                    if (j == 1)
                    {
                        Console.Write(Constants.FormatSettings, dataTable.Rows[0][parameters[singsi]]);
                        singsName[singsi] = dataTable.Rows[0][parameters[singsi]].ToString();
                    }
                    
                    Console.Write(Constants.FormatSettings, dataTable.Rows[j][parameters[singsi]]);
                    sings[singsi][singsj] = Convert.ToDouble(dataTable.Rows[j][parameters[singsi]]);
                }
                
                Console.WriteLine();
            }
            
            int attackTypeColumnIndex = default;
            for (int i = 0; i < dataTable.Rows[0].ItemArray.Length; i++)
            {
                if (dataTable.Rows[0][i].ToString() == "AttackType")
                {
                    attackTypeColumnIndex = i;
                }
            }

            if (attackTypeColumnIndex == default)
            {
                throw new Exception();
            }
            
            int[] ck = new int[Constants.LengthSings];
            for (int i = 1, actualIndex = 0; i <= ck.Length; ++i, ++actualIndex)
            {
                ck[actualIndex] = dataTable.Rows[i][attackTypeColumnIndex].ToString() == "Normal" ? 0 : 1;
            }

            Console.Write("\n \n \n");
            
            return (sings, ck, singsName);
        }
    }

    public static double[][] AllocateMemory(int rows, int columns)
    {
        var array = new double[rows][];
       
        for (int i = 0; i< rows; i++)
        {
            array[i] = new double[columns];
        }

        return array;
    }

    public static void PrintOneDimension(double[] values, string[] singsName)
    {
        Console.WriteLine();
        for (int i =0; i< values.Length; i++)
        {
            Console.Write(Constants.FormatSettings, singsName[i]);
            Console.Write(Constants.FormatSettings, values[i]);
            Console.WriteLine();
        }
        
        Console.WriteLine();
    }
    
    public static void PrintOneDimension(int[] values, bool withNewLine = false)
    {
        for (int i =0; i< values.Length; i++)
        {
            Console.Write(Constants.FormatSettings, values[i]);
            
            if (withNewLine)
            {
                Console.WriteLine();
            }
        }
        
        Console.WriteLine();
    }
    
    public static void PrintOneDimension(int[] values)
    {
        Console.Write(Constants.FormatSettings, "AttackType");
        for (int i =0; i< values.Length; i++)
        {
            Console.Write(Constants.FormatSettings, values[i]);
        }
        
        Console.WriteLine();
    }

    public static void PrintTwoDimension(double[][] sings)
    {
        Console.WriteLine();
        for (int i = 0; i < sings.Length; i++)
        {
            for (int j = 0; j < sings[0].Length; j++)
            {
                Console.Write(Constants.FormatSettings, sings[i][j]);
            }
            Console.WriteLine();
        }
        
        Console.WriteLine();
    }
    
    public static void PrintTwoDimension(double[][] sings, string[] singNames)
    {
        Console.Write(Constants.FormatSettings," ");
        for (int i = 0; i < sings[0].Length; i++)
        {
            Console.Write(Constants.FormatSettings,$"index{i}");
        }

        Console.WriteLine();
        
        for (int i = 0; i < sings.Length; i++)
        {
            Console.Write(Constants.FormatSettings, singNames[i]);
            for (int j = 0; j < sings[0].Length; j++)
            {
                Console.Write(Constants.FormatSettings, sings[i][j]);
            }

            Console.WriteLine();
        }

        Console.WriteLine();
    }
    
    public static void PrintFrequencies(double[][] sings, string[] singNames)
    {
        Console.Write(Constants.FormatSettings, "-");
        Console.Write(Constants.FormatSettings, "00/NN");
        Console.Write(Constants.FormatSettings, "01/NA");
        Console.Write(Constants.FormatSettings, "11/AA");
        Console.Write(Constants.FormatSettings, "10/AN");
        Console.WriteLine();
        for (int i = 0; i < sings.Length; i++)
        {
            Console.Write(Constants.FormatSettings, singNames[i]);
            for (int j = 0; j < sings[0].Length; j++)
            {
                Console.Write(Constants.FormatSettings, sings[i][j]);
            }
            Console.WriteLine();
        }
    }

    /// <summary>
    /// Поиск минимального и максимального значения для каждого признака.
    /// </summary>
    /// <param name="sings">Признаки.</param>
    /// <returns>Минимальное и максимальное значение для каждого признака.</returns>
    public static double[][] FindMinMax(double[][] sings)
    {
        var maxMinSings = new double[sings.Length][];
        for (int i = 0; i< sings.Length; i++)
        {
            // Первый индекс max, второй min
            maxMinSings[i] = new double[2] { sings[i][0], sings[i][0] };
        }

        for (int i = 0; i < sings.Length; i++)
        {
            for (int j = 0; j < sings[0].Length; j++)
            {
                if (sings[i][j] > maxMinSings[i][0])
                {
                    maxMinSings[i][0] = sings[i][j];
                }

                if (sings[i][j] < maxMinSings[i][1])
                {
                    maxMinSings[i][1] = sings[i][j];
                }
            }
        }

        return maxMinSings;
    }


    /// <summary>
    /// Нормализация признаков.
    /// </summary>
    /// <param name="sings">Признаки.</param>
    /// <param name="maxMinSings">Максимальные и минимальные значение признаков.</param>
    /// <returns>Нормализованный список признаков.</returns>
    public static double[][] NormalizeSings(double[][] sings, double[][] maxMinSings)
    {
        var normalazedSings = AllocateMemory(sings.Length, sings[0].Length);

        for (int i = 0; i < sings.Length; i++)
        {
            for (int j = 0; j < sings[0].Length; j++) 
            {
                normalazedSings[i][j] = Math.Round((sings[i][j] - maxMinSings[i][1]) / (maxMinSings[i][0] - maxMinSings[i][1]), Constants.Accuracy);
            }
        }

        return normalazedSings;
    }

    /// <summary>
    /// Поиск среднего значения у границ.
    /// </summary>
    /// <param name="maxMinSings">Максимальные и минимальные значения каждого признака.</param>
    /// <returns>Среднее значение</returns>
    public static double[] FindMiddleMaxMinSings(double[][] maxMinSings)
    {
        double[] middleMaxMinSings = new double[maxMinSings.Length];

        for (int i = 0; i < maxMinSings.Length; i++)
        {
            middleMaxMinSings[i] = Math.Round(((maxMinSings[i][0]+maxMinSings[i][1]) / 2), Constants.Accuracy);
        }

        return middleMaxMinSings;
    }

    /// <summary>
    /// Частота появления признаков для заданного класса.
    /// Есть алгоритм покрытия
    /// </summary>
    /// <param name="sings"></param>
    /// <returns></returns>
    /// c1 NN 00/ c2 NA 01 / c3 AA 11 / c4 AN 10
    public static double[][] FrequencyOfSingsOccurrences(double[][] normalizedSings, int[] ck, double[] middleValues)
    {
        double[][] frequencies = AllocateMemory(normalizedSings.Length, 4);
        for (int i = 0; i < normalizedSings.Length; i++)
        {
            for (int j = 0; j < normalizedSings[0].Length; j++)
            {
                if (normalizedSings[i][j] <= 0.5 && ck[j] == 0)
                {
                    ++frequencies[i][0];
                }
                else if (normalizedSings[i][j] <= 0.5 && ck[j] == 1)
                {
                    ++frequencies[i][1];
                }
                else if (normalizedSings[i][j] > 0.5 && ck[j] == 0)
                {
                    ++frequencies[i][2];
                }
                else if (normalizedSings[i][j] > 0.5 && ck[j] == 1)
                {
                    ++frequencies[i][3];
                }
            }
        }
        
        return frequencies;
    }

    /// <summary>
    /// Подсчет числа векторов заданного класса во множестве данных
    /// </summary>
    /// <param name="normalizedSings"></param>
    /// <param name="ck"></param>
    /// <returns></returns>
    public static (int, int) VectorCounts(double[][] normalizedSings, int[] ck)
    {
        int[] indexes = new int[normalizedSings[0].Length];
        for (int i = 0; i < indexes.Length; ++i)
        {
            indexes[i] = i;
        }

        int normalCount = default;
        int attackCount = default;
        for (int j = 0; j < normalizedSings[0].Length; ++j)
        {
            if (ck[j] == 0 && indexes.Contains(j))
            {
                normalCount++;
            }
            else if (ck[j] == 1 && indexes.Contains(j))
            {
                attackCount++;
            }
        }
        
        return (normalCount, attackCount);
    }
    
    // Выбор вершины с наибольшей частотой появление признака для заданного класса
    public static (double[][], string[], string, string, string) ExtractMaxFrequenciesOfClassSings(double[][] frequencies, string[] singsName)
    {
        int mostFriquentSingsClassRow = -1;
        int mostFriquentSingsClassColumn = -1;
        double maxEl = -1;

        for (int i = 0; i < frequencies.Length; i++)
        {
            for (int j = 0; j < frequencies[0].Length; j++)
            {
                if (frequencies[i][j] > maxEl)
                {
                    maxEl = frequencies[i][j];

                    mostFriquentSingsClassRow = i;
                    mostFriquentSingsClassColumn = j;
                }
            }
        }
        
        string rulesWithName = $"{singsName[mostFriquentSingsClassRow]} {Constants.SingTypes[mostFriquentSingsClassColumn]}";
        string rules = $"{Constants.SingTypes[mostFriquentSingsClassColumn]}";
        
        Console.WriteLine($"Найден признак с наибольшим количеством значений в классе - это ${singsName[mostFriquentSingsClassRow]} \n" +
                          $"Тип {Constants.SingTypes[mostFriquentSingsClassColumn]} \n" +
                          $"Формируем правило {rulesWithName} \n" +
                          $"Количество однордных значений признаков соотв классу {maxEl} \n" +
                          "Исключаем его из множества \n");

        var extractedFrequencies = AllocateMemory(frequencies.Length - 1, frequencies[0].Length);
        var extractedSingNames = new string[singsName.Length - 1];
        for (int i = 0, k = 0; i < frequencies.Length; i++)
        {
            if (i != mostFriquentSingsClassRow) // Убираем ненужную строку
            {
                extractedSingNames[k] = singsName[i];
                for (int j = 0; j < frequencies[0].Length; j++)
                {
                    extractedFrequencies[k][j] = frequencies[i][j];
                }
                ++k;
            }
        }
        
        return (extractedFrequencies, extractedSingNames, rulesWithName, $"{singsName[mostFriquentSingsClassRow]}", rules);
    }

    public static (string, string) ParseRule(string rule)
    {
        string ruleOperator = ($"{rule[0]}{rule[1]}").Trim();
        string type = string.Empty;
        for (int i = rule.Length - 7; i < rule.Length - 1; i++)
        {
            type += rule[i];
        }

        return (ruleOperator, type);
    }

    public static int[] Tree(double[][] sings, int[] topOrder, string[] rules)
    {
        string[] types = new string[rules.Length];
        string[] ruleOperators = new string[rules.Length];
        
        for (int i = 0; i < rules.Length; i++)
        {
            (ruleOperators[i], types[i]) = ParseRule(rules[i]);
        }

        int[] stackResults = new int[Constants.LengthSings];
        for (int i = 0; i < sings[0].Length; i++)
        {
            string[] innerVector = new string[sings[0].Length];
            
            for (int j = 0; j < sings.Length; j++)
            {
                if (Compare(sings[topOrder[j]][i], 0.5, ruleOperators[j]))
                {
                    innerVector[j] = types[j].Contains("Attack") ? "A" : "N";
                }
                else
                {
                    innerVector[j] = types[j] switch
                    {
                        "Attack" => "N",
                        "Normal" => "A",
                        _ => innerVector[j]
                    };
                }
            }

            stackResults[i] = innerVector.Count(a => a == "A") > innerVector.Count(a => a == "N")
                ? 1
                : 0;
        }

        return stackResults;
    }

    public static void CompareClassSingsResultWithOriginal(int[] original, int[] result)
    {
        int matchedClassSings = 0; // Совпали
        int tp = 0;
        int tn = 0;

        int fp = 0;
        int fn = 0;

        for (int i = 0; i < original.Length; i++)
        {
            if (original[i] == 0 && result[i] == 0) // Оба Normal
            {
                ++tn;
            }
            else if (original[i] == 1 && result[i] == 1) //  Оба Attack
            {
                ++tp;
            }
            else if (original[i] == 0 && result[i] == 1) // Ориг Normal результат Attack
            {
                ++fp;
            }
            else if (original[i] == 1 && result[i] == 0)// Ориг Attack результат Normal
            {
                ++fn;
            }
        }

        Console.WriteLine($"True Positive {tp}");
        Console.WriteLine($"True Negative {tn}");
        Console.WriteLine($"False Positive {fp}");
        Console.WriteLine($"False Negative {fn}");

        double accuracy = Math.Round((double)(tp + tn) / (tp + tn + fp + fn), Constants.Accuracy);
        Console.WriteLine($"Точность равна {accuracy*100}%");

        var tpr = Math.Round((double)tp / (tn + fp), Constants.Accuracy);
        Console.WriteLine($"Чувствительность {tpr * 100}%");

        var tnr = Math.Round((double)tn / (tn + fp), Constants.Accuracy);
        Console.WriteLine($"Специфичность равна {tnr* 100}%");

        var fdr = Math.Round((double)fp / (fp + tp), Constants.Accuracy);
        Console.WriteLine($"Частота ложных обнаружений равна {fdr*100}%");

        var forSpicificator = Math.Round((double) fn / (fn+tn), Constants.Accuracy);
        Console.WriteLine($"Частота ложных пропусков равна {forSpicificator* 100}%");
    }

    public static bool Compare(double a, double b, string op)
    {
        switch (op)
        {
            case ">":
                return a > b;
            break;
            case "<":
                return a < b;
            break;
            case ">=":
                return a >= b;
            break;
            case "<=":
                return a <= b;
            break;
        }

        return false;   
    }
}

//Dictionary<string, List<double>> sings = new Dictionary<string, List<double>>();
