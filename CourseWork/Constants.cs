﻿namespace CourseWork
{
    public static class Constants
    {
        public static int SingsCount = 0; // Количество признаков
        public static readonly int LengthSings = 140; // Длина признака

        public static string FormatSettings = "{0,-25}";
        public static int Accuracy = 5;

        public static string Line = "-----------------------------------";

        public static string[] SingTypes = new[]
            { "<= 0.5 (Normal Normal)", "<= 0.5 (Normal Attack)", "> 0.5 (Attack Normal)", "> 0.5 (Attack Attack)" };
    }
}

